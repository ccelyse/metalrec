<!DOCTYPE html>

<html lang="en-US">

<head>
	<script src="cdn-cgi/apps/head/_jZ0jIuSj0S4kK9BleaWviaKHSc.js"></script>
	<script type="text/javascript">
        document.documentElement.className = document.documentElement.className + ' yes-js js_active js'
	</script>

	<link type="text/css" media="all" href="wp-content/cache/autoptimize/1/css/autoptimize_a1c26e2b6904c7a320d895dbcc6cddf8.css" rel="stylesheet" />
	<link type="text/css" media="only screen and (max-width: 768px)" href="wp-content/cache/autoptimize/1/css/autoptimize_3be592c671f598b43e637562b04345ed.css" rel="stylesheet" />
	<title>Metalrec</title>

	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />

	<link rel="pingback" href="http://uplift.swiftideas.com/xmlrpc.php" />
	<script type="text/javascript">
        var yith_wcwl_plugin_ajax_web_url = '/wp-admin/admin-ajax.php';
	</script>
	<script>
        function sf_writeCookie() {
            the_cookie = document.cookie, the_cookie && window.devicePixelRatio >= 2 && (the_cookie = "sf_pixel_ratio=" + window.devicePixelRatio + ";" + the_cookie, document.cookie = the_cookie)
        }
        sf_writeCookie();
	</script>
	<link rel='dns-prefetch' href='http://fonts.googleapis.com' />
	<link rel='dns-prefetch' href='http://s.w.org' />
	<link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Feed" href="feed/index.rss" />
	<link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Comments Feed" href="comments/feed/index.rss" />
	<link rel="alternate" type="application/rss+xml" title="Uplift &raquo; Home: Classic Comments Feed" href="feed/index.rss" />

	<meta property="og:title" content="Metalrec" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://uplift.swiftideas.com/home/home-classic/" />
	<meta property="og:site_name" content="Metalrec" />
	<meta property="og:description" content="">
	<meta property="og:image" content="wp-content/uploads/2014/12/uplift_logo.png" />

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="Metalrec">
	<meta name="twitter:description" content="">
	<meta property="twitter:image:src" content="wp-content/uploads/2014/12/uplift_logo.png" />
	<script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "http:\/\/uplift.swiftideas.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.6"
            }
        };
        ! function(a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case "flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case "emoji":
                        return b = d([55357, 56692, 8205, 9792, 65039], [55357, 56692, 8203, 9792, 65039]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }
            var g, h, i, j, k = b.createElement("canvas"),
                l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function() {
                c.DOMReady = !0
            }, c.supports.everything || (h = function() {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function() {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
	</script>
	<link rel='stylesheet' id='redux-google-fonts-sf_uplift_options-css' href='http://fonts.googleapis.com/css?family=Lato%3A100%2C300%2C400%2C700%2C900%2C100italic%2C300italic%2C400italic%2C700italic%2C900italic%7COpen+Sans%3A300%2C400%2C600%2C700%2C800%2C300italic%2C400italic%2C600italic%2C700italic%2C800italic&amp;subset=latin&amp;ver=1474553898' type='text/css' media='all' />
	<script type='text/javascript' src='wp-includes/js/jquery/jquery-ver=1.12.4.js'></script>
	<script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min-ver=1.4.1.js'></script>
	<script type='text/javascript' src='wp-content/themes/uplift/js/combine/plyr.js'></script>
	<link rel='https://api.w.org/' href='wp-json/index.json' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php-rsd.xml" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
	<link rel="canonical" href="index.html" />
	<link rel='shortlink' href='index.html' />
	<link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed-url=http-%7C%7Cuplift.swiftideas.com%7Chome%7Chome-classic%7C.json" />
	<link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed-url=http-%7C%7Cuplift.swiftideas.com%7Chome%7Chome-classic%7C&amp;format=xml.xml" />
	<script type="text/javascript">
        var ajaxurl = 'http://uplift.swiftideas.com/wp-admin/admin-ajax.php';
	</script>
	<!--[if lt IE 9]><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/respond.js"></script><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/html5shiv.js"></script><script data-cfasync="false" src="http://uplift.swiftideas.com/wp-content/themes/uplift/js/excanvas.compiled.js"></script><![endif]-->

</head>

<body class="page-template-default page page-id-13177 page-child parent-pageid-9  minimal-design mobile-header-center-logo mhs-tablet-land mh-sticky  mh-overlay responsive-fluid sticky-header-enabled page-shadow mobile-two-click standard product-shadows header-standard layout-fullwidth has-quickview-hover-btn disable-mobile-animations ">
<div id="site-loading" class="circle">
	<div class="sf-svg-loader">
		<object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object>
	</div>
</div>


<div id="container">
	<div id="mobile-menu-wrap" class="menu-is-left">
		<nav id="mobile-menu" class="clearfix">
			<div class="menu-main-menu-container">
				<ul id="menu-main-menu" class="menu">
					<li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">
						<a href="index.html"><span class="menu-item-text">ACCUEIL</span></a>
					</li>
					<!--<li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">-->
						<!--<a href="#"><span class="menu-item-text">NOUS CONNAÎTRE</span></a>-->
					<!--</li>-->
					<!--<li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">-->
						<!--<a href="CEQUEVOUSOFFREMETALREC.html"><span class="menu-item-text">CE QUE VOUS OFFRE METALREC</span></a>-->
					<!--</li>-->
					<li class="menu-item-14712 menu-item menu-item-type-post_type menu-item-object-page menu-item-home current_page_ancestor">
						<a href="contact.html"><span class="menu-item-text">CONTACT</span></a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<header id="mobile-header" class="mobile-center-logo clearfix">
		<div class="mobile-header-opts opts-left">
			<button class="hamburger mobile-menu-link hamburger--3dy" type="button">
                    <span class="hamburger-box">
<span class="hamburger-inner"></span>
                    </span>
			</button>
		</div>
		<div id="mobile-logo" class="logo-center has-img clearfix" data-anim="tada">
			<a href="index.html">
				<img class="standard" src="images/metalrec.png" alt="Uplift" height="30" width="77" />
				<img class="retina" src="images/metalrec.png" alt="Uplift" height="30" width="77" />
				<div class="text-logo"></div>
			</a>
		</div>
	</header>

	<div class="header-wrap  full-center full-header-stick page-header-standard" data-style="default" data-default-style="default">
		<div id="header-section" class="header-3 ">
			<header id="header" class="sticky-header clearfix">
				<div id="sf-full-header-search">
					<div class="container">
						<form method="get" class="header-search-form" action="http://uplift.swiftideas.com/">
							<input type="text" placeholder="Type and hit enter to search" name="s" autocomplete="off" />
						</form>
						<a href="index.html#" class="sf-fhs-close"><i class="sf-icon-remove-big"></i></a>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div id="logo" class="logo-left has-img clearfix" data-anim="tada">
							<a href="index.html">
								<img class="standard" src="images/metalrec.png" alt="Uplift"/>
								<img class="retina" src="images/metalrec.png" alt="Uplift"  />
								<div class="text-logo"></div>
							</a>
						</div>
						<div class="float-menu">
							<nav id="main-navigation" class="std-menu clearfix">
								<div id="mega-menu-wrap-main_navigation" class="mega-menu-wrap">
									<div class="mega-menu-toggle">
										<div class='mega-toggle-block mega-menu-toggle-block mega-toggle-block-right' id='mega-toggle-block-1'></div>
									</div>
									<ul id="mega-menu-main_navigation" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-document-click="collapse" data-reverse-mobile-items="true" data-vertical-behaviour="standard" data-breakpoint="600">
										<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>
											<a class="mega-menu-link" href="index.html">ACCUEIL</a>
										</li>
										<!--<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>-->
											<!--<a class="mega-menu-link" href="#">NOUS CONNAÎTRE</a>-->
										<!--</li>-->
										<!--<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>-->
											<!--<a class="mega-menu-link" href="CEQUEVOUSOFFREMETALREC.html">CE QUE VOUS OFFRE METALREC</a>-->
										<!--</li>-->
										<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-home mega-current-page-ancestor mega-current-menu-parent' id='mega-menu-item-14712'>
											<a class="mega-menu-link" href="contact.html">CONTACT</a>
										</li>

									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</header>
		</div>
	</div>
	<div id="sf-mobile-slideout-backdrop"></div>


	<div id="main-container" class="clearfix">
		<div class="inner-container-wrap" id="contact">
			<div class="inner-page-wrap has-no-sidebar no-top-spacing clearfix">

				<div class="clearfix">
					<div class="page-content hfeed clearfix">
						<div class="clearfix post-13072 page type-page status-publish hentry" id="13072">
							<section class="row fw-row ">
								<div class="spb_gmaps_widget fullscreen-map spb_content_element col-sm-12">
									<div class="spb-asset-content">
										<div class="spb_map_wrapper">

										</div>
									</div>
								</div>
							</section>
							<section class="container ">
								<div class="row">
									<div class="blank_spacer col-sm-12 " style="height:60px;"></div>
								</div>
							</section>
							<section class="container ">
								<div class="row">
									<!--<h1 class="entry-title" style="text-align: center;font-weight: bold;text-transform: uppercase;">QUELQUE CHOSE À DIRE? ALORS PASSER UNE LIGNE</h1>-->
									<div class="spb-column-container col-sm-12" style="padding-left:15px; padding-right:15px; ">
										<div class="spb-asset-content" style="">
											<section class="container ">
												<div class="row">
													<div class="spb_content_element col-sm-12 spb_text_column">
														<div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
															<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2520.140103199466!2d4.359860215379425!3d50.82856876792866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c3c48c9d2fd31f%3A0xbc0978d0223db9de!2sAvenue+Louise+149%2F24%2C+1050+Bruxelles%2C+Belgium!5e0!3m2!1sen!2srw!4v1566250147510!5m2!1sen!2srw" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

														</div>
													</div>
												</div>
											</section>
										</div>
									</div>
									<div class="spb_content_element col-sm-6 spb_text_column">
										<div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
												<div class="screen-reader-response"></div>
												<form action="sendmail.php" method="post" class="wpcf7-form">
													<div style="display: none;">
														<input type="hidden" name="_wpcf7" value="15387" />
														<input type="hidden" name="_wpcf7_version" value="4.4.2" />
														<input type="hidden" name="_wpcf7_locale" value="en_US" />
														<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f15387-p13072-o1" />
														<input type="hidden" name="_wpnonce" value="7ae6c8aa2d" />
													</div>
													<p><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Name" /></span>
														<br />
														<span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-invalid="false" placeholder="Address" /></span>
														<br />
														<span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Subject" /></span>
														<br />
														<span class="wpcf7-form-control-wrap message"><textarea name="message" cols="40" rows="5" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span>
														<br />
														<input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit" />
													</p>
													<div class="wpcf7-response-output wpcf7-display-none"></div>
												</form>
											</div>
										</div>
									</div>
									<div class="spb-column-container col-sm-6" style="padding-left:15px; padding-right:15px; ">
										<div class="spb-asset-content" style="">
											<section class="container ">
												<div class="row">
													<!--<h3 class="spb-heading"><span>Get in touch</span></h3></div>-->
												<div class="textwidget">Avenue Louise 149/24 1050 BRUXELLES BELGIEN
													<br> Tél : + 32 4 6 8184070
													<br> Email: <a href="#"><span class="__cf_email__">contact.metalrec@gmail.com</span></a>
													<br>
													<br>
													<!--<ul class="social-icons standard ">-->
														<!--<li class="twitter"><a href="#" target="_blank"><i class="fa-twitter"></i><i class="fa-twitter"></i></a></li>-->
														<!--<li class="facebook"><a href="#" target="_blank"><i class="fa-facebook"></i><i class="fa-facebook"></i></a></li>-->
														<!--&lt;!&ndash;<li class="dribbble"><a href="#" target="_blank"><i class="fa-dribbble"></i><i class="fa-dribbble"></i></a></li>&ndash;&gt;-->
														<!--<li class="linkedin"><a href="#" target="_blank"><i class="fa-linkedin"></i><i class="fa-linkedin"></i></a></li>-->
														<!--<li class="instagram"><a href="#" target="_blank"><i class="fa-instagram"></i><i class="fa-instagram"></i></a></li>-->
													<!--</ul>-->
												</div>
												</div>
											</section>
										</div>
									</div>
								</div>
							</section>
							<div class="link-pages"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div id="sf-full-header-search-backdrop"></div>

	</div>
	<div id="footer-wrap">

		<footer id="footer">
			<div class="container">
				<div id="footer-widgets" class="row clearfix">
					<div class="col-md-6">
						<section id="text-3" class="widget widget_text clearfix" style="display: inline-grid;">
							<div class="textwidget">
								<img class="alignleft size-full wp-image-14782" src="images/metalrec.png" alt="uplift_logo_white" />
							</div>
							<p style="display: inline-grid;">
								<strong>©2019 METALREC</strong>
							</p>
						</section>
					</div>


					<div class="col-md-6" style="display: flex;justify-content: flex-end;">
						<section id="text-5" class="widget widget_text clearfix">
							<div class="widget-heading title-wrap clearfix">
								<h3 class="spb-heading"><span>Get in touch</span></h3></div>
							<div class="textwidget">Avenue Louise 149/24 1050 BRUXELLES BELGIEN
								<br> Tél : + 32 4 6 8184070
								<br> Email: <a href="#"><span class="__cf_email__">contact.metalrec@gmail.com</span></a>
								<br>
								<br>
								<ul class="social-icons standard ">
									<li class="twitter"><a href="#" target="_blank"><i class="fa-twitter"></i><i class="fa-twitter"></i></a></li>
									<li class="facebook"><a href="#" target="_blank"><i class="fa-facebook"></i><i class="fa-facebook"></i></a></li>
									<!--<li class="dribbble"><a href="#" target="_blank"><i class="fa-dribbble"></i><i class="fa-dribbble"></i></a></li>-->
									<li class="linkedin"><a href="#" target="_blank"><i class="fa-linkedin"></i><i class="fa-linkedin"></i></a></li>
									<li class="instagram"><a href="#" target="_blank"><i class="fa-instagram"></i><i class="fa-instagram"></i></a></li>
								</ul>
							</div>
						</section>
					</div>
				</div>
			</div>

		</footer>
	</div>

</div>

<div id="back-to-top" class="animate-top"><i class="sf-icon-up-chevron"></i></div>

<div class="fw-video-area">
	<div class="fw-video-close"><i class="sf-icon-remove"></i></div>
	<div class="fw-video-wrap"></div>
</div>
<div class="fw-video-spacer"></div>

<div id="sf-included" class="has-products has-carousel has-parallax has-team stickysidebars "></div>
<div id="sf-option-params" data-slider-slidespeed="7000" data-slider-animspeed="600" data-slider-autoplay="0" data-slider-loop="" data-carousel-pagespeed="800" data-carousel-slidespeed="200" data-carousel-autoplay="0" data-carousel-pagination="0" data-lightbox-nav="default" data-lightbox-thumbs="1" data-lightbox-skin="light" data-lightbox-sharing="1" data-product-zoom-type="lens" data-product-slider-thumbs-pos="left" data-product-slider-vert-height="659" data-quickview-text="Quickview" data-cart-notification="tada" data-username-placeholder="Username" data-email-placeholder="Email" data-password-placeholder="Password" data-username-or-email-placeholder="Username or email address" data-order-id-placeholder="Order ID" data-billing-email-placeholder="Billing Email"></div>
<div class="sf-svg-loader">
	<object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object>
</div>
<div id="loveit-locale" data-ajaxurl="http://uplift.swiftideas.com/wp-admin/admin-ajax.php" data-nonce="2e85738f5b" data-alreadyloved="You have already loved this item." data-error="Sorry, there was a problem processing your request." data-loggedin="false"></div>
<script data-cfasync="false" src="cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script>
<script type="text/javascript">
    setTimeout(function() {
        var a = document.createElement("script");
        var b = document.getElementsByTagName('script')[0];
        a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0049/1794.js";
        a.async = true;
        a.type = "text/javascript";
        b.parentNode.insertBefore(a, b)
    }, 1);
</script>
<div class="sf-container-overlay">
	<div class="sf-loader">
		<div class="sf-svg-loader">
			<object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object>
		</div>
	</div>
</div>
<script type="text/javascript">
    var onLoad = {
        init: function() {

            "use strict";

            // Variables
            var trigger = jQuery('#sf-styleswitch-trigger'),
                triggerWidth = trigger.width(),
                switcher = jQuery('#sf-style-switcher'),
                switchCont = switcher.find('.switch-cont'),
                isAnimating = false;

            // Loaded
            trigger.css('width', '60px').fadeIn(400).addClass('loaded');

            // Hove in/out
            trigger.on('mouseover', function() {
                if (switcher.hasClass('open') || switcher.hasClass('animating')) {
                    return;
                }
                switcher.css('width', '');
                trigger.transition({
                    width: triggerWidth
                }, 400, 'easeOutCirc');
            });
            trigger.on('mouseleave', function() {
                if (switcher.hasClass('open')) {
                    return;
                }
                if (!switcher.hasClass('open') && switcher.hasClass('animating')) {
                    setTimeout(function() {
                        trigger.transition({
                            width: '60'
                        }, 400, 'easeOutCirc');
                    }, 600);
                } else {
                    trigger.transition({
                        width: '60'
                    }, 400, 'easeOutCirc');
                }
            });

            // Open switcher window
            trigger.on('click', function(e) {

                e.preventDefault();

                if (isAnimating) {
                    return;
                }

                isAnimating = true;
                switcher.addClass('animating');

                switcher.toggleClass('open');
                setTimeout(function() {
                    isAnimating = false;
                    switcher.removeClass('animating');
                }, 600);
            });
            // Close switcher window

            // Switcher controls

            if (jQuery('#header-section').length > 0) {
                var currentHeader = jQuery('#header-section').attr('class').split(' ')[0];
                jQuery(".header-select option[value=" + currentHeader + "]").prop("selected", "selected")
            }

            jQuery('.header-select').change(function() {
                var baseURL = onLoad.getPathFromUrl(location.href),
                    newURLParam = "?header=" + jQuery('.header-select').val();

                location.href = baseURL + newURLParam;
            });

            jQuery('.color-select li').on('click', 'a', function(e) {
                e.preventDefault();

                jQuery('.color-select li').removeClass('active');
                jQuery(this).parent().addClass('active');

                var selectedColor = '#' + jQuery(this).data('color');
                var s = "#ff0000";
                var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
                var matches = patt.exec(selectedColor);
                var top = "rgba(" + parseInt(matches[1], 16) + "," + parseInt(matches[2], 16) + "," + parseInt(matches[3], 16) + ",0.60)";
                var bottom = "rgba(" + parseInt(matches[1], 16) + "," + parseInt(matches[2], 16) + "," + parseInt(matches[3], 16) + ",1.0)";

                // background-color
                jQuery('.sf-accent-bg, .funded-bar .bar, .flickr-widget li, .portfolio-grid li, figcaption .product-added, .woocommerce .widget_layered_nav ul li.chosen small.count, .woocommerce .widget_layered_nav_filters ul li a, span.highlighted, #one-page-nav li .hover-caption, #sidebar-progress-menu ul li.reading .progress, .loading-bar-transition .pace .pace-progress, input[type=submit], button[type=submit], input[type="file"], .wpcf7 input.wpcf7-submit[type=submit], .sf-super-search .search-options .ss-dropdown ul, av ul.menu > li.menu-item.sf-menu-item-btn > a, .shopping-bag-item a > span.num-items, .bag-buttons a.checkout-button, .bag-buttons a.create-account-button, .woocommerce input.button.alt, .woocommerce .alt-button, .woocommerce button.button.alt, #jckqv .cart .add_to_cart_button, #fullscreen-supersearch .sf-super-search .search-go a.sf-button, #respond .form-submit input[type=submit], .sf-button.accent:not(.bordered), .sf-icon-box-animated .back, .spb_icon_box_grid .spb_icon_box .divider-line, .tabs-type-dynamic .nav-tabs li.active a, .progress .bar, .mejs-controls .mejs-time-rail .mejs-time-current, .team-member-divider, .masonry-items li.testimonial .testimonial-text, .spb_tweets_slider_widget .tweet-icon i, .woocommerce .cart button.add_to_cart_button.product-added, .woocommerce .single_add_to_cart_button:disabled[disabled], .woocommerce .order-info, .woocommerce .order-info mark, .woocommerce .button.checkout-button, .woocommerce #review_form #respond .form-submit input, .woocommerce button[type="submit"], .woocommerce input.button, .woocommerce a.button, .woocommerce-cart table.cart input.button, .review-order-wrap #payment #place_order, #buddypress .pagination-links span, #buddypress .load-more.loading a, #bbp-user-navigation ul li.current a, .bbp-pagination-links span.current').css('background-color', selectedColor);
                // color
                jQuery('.sf-accent, .portfolio-item .portfolio-item-permalink, .read-more-link, .blog-item .read-more, .author-link, span.dropcap2, .spb_divider.go_to_top a, #header-translation p a, span.dropcap4, #sidebar-progress-menu ul li.reading a, .read-more-button, .player-controls button.tab-focus, .player-progress-played[value], .sf-super-search .search-options .ss-dropdown > span, .sf-super-search .search-options input, .author-bio a.author-more-link, .comment-meta-actions a, .blog-aux-options li.selected a, .sticky-post-icon, .blog-item .author a.tweet-link, .side-post-info .post-share .share-link, a.sf-button.bordered.accent, .progress-bar-wrap .progress-value, .sf-share-counts .share-text h2, .sf-share-counts .share-text span, .woocommerce div.product .stock, .woocommerce form .form-row .required, .woocommerce .widget_price_filter .price_slider_amount .button, .product-cat-info a.shop-now-link, .woocommerce-cart table.cart input[name="apply_coupon"], .woocommerce .shipping-calculator-form button[type="submit"], .woocommerce .cart input.button[name="update_cart"]').css('color', selectedColor);
                // border-color
                jQuery('.sf-accent-border,span.dropcap4, .super-search-go, .sf-button.accent, .sf-button.accent.bordered .sf-button-border, blockquote.pullquote, .woocommerce form .form-row.woocommerce-invalid .select2-container, .woocommerce form .form-row.woocommerce-invalid input.input-text, .woocommerce form .form-row.woocommerce-invalid select, .woocommerce .woocommerce-info, .woocommerce-page .woocommerce-info, .woocommerce-cart table.cart input[name="apply_coupon"], .woocommerce .shipping-calculator-form button[type="submit"], .woocommerce .cart input.button[name="update_cart"], #buddypress .activity-header a, #buddypress .activity-read-more a, #buddypress .pagination-links span, #buddypress .load-more.loading a, #bbp-user-navigation ul li.current a, .bbp-pagination-links span.current').css('border-color', selectedColor);
                jQuery('.spb_impact_text .spb_call_text, code > pre').css('border-left-color', selectedColor);
                jQuery('#account-modal .nav-tabs li.active span, .sf-super-search .search-options .ss-dropdown > span, .sf-super-search .search-options input').css('border-bottom-color', selectedColor);
                jQuery('#bbpress-forums li.bbp-header').css('border-top-color', selectedColor);
                // stroke
                jQuery('.sf-hover-svg path').css('stroke', selectedColor);

                console.log('-webkit-gradient(linear,left top,left bottom,from(' + top + ') 25%,to(' + bottom + ') 100%)');
                jQuery('figure.animated-overlay figcaption').css('background', '-webkit-gradient(linear,left top,left bottom,color-stop(25%,' + top + '),to(' + bottom + '))');
                jQuery('figure.animated-overlay figcaption').css('background', '-webkit-linear-gradient(top,' + top + ' 25%, ' + bottom + ' 100%)');
                jQuery('figure.animated-overlay figcaption').css('background', 'linear-gradient(to bottom,' + top + ' 25%, ' + bottom + ' 100%)');

                jQuery('figcaption .thumb-info-alt > i, .gallery-item figcaption .thumb-info > i, .gallery-hover figcaption .thumb-info > i').css('color', selectedColor);

            });

        },
        getURLVars: function() {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getPathFromUrl: function(url) {
            return url.split("?")[0];
        }
    };

    jQuery(document).ready(onLoad.init);
</script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/core.min-ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/widget.min-ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/button.min-ver=1.11.4.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/spinner.min-ver=1.11.4.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var jckqv = {
        "ajaxurl": "http:\/\/uplift.swiftideas.com\/wp-admin\/admin-ajax.php",
        "nonce": "6ed2845eef",
        "settings": {
            "styling_autohide": "0",
            "styling_hoverel": ".product",
            "styling_icon": "eye",
            "styling_text": "Quickview",
            "styling_btnstyle": "flat",
            "styling_padding": ["8", "10", "8", "10"],
            "styling_btncolour": "#66cc99",
            "styling_btnhovcolour": "#47C285",
            "styling_btntextcolour": "#ffffff",
            "styling_btntexthovcolour": "#ffffff",
            "styling_borderradius": ["4", "4", "4", "4"],
            "position_autoinsert": "1",
            "position_position": "afteritem",
            "position_align": "left",
            "position_margins": ["0", "0", "10", "0"],
            "general_method": "click",
            "imagery_imgtransition": "horizontal",
            "imagery_transitionspeed": "600",
            "imagery_autoplay": "0",
            "imagery_autoplayspeed": "3000",
            "imagery_infinite": "1",
            "imagery_navarr": "1",
            "imagery_thumbnails": "thumbnails",
            "content_showtitle": "1",
            "content_showprice": "1",
            "content_showrating": "1",
            "content_showbanner": "1",
            "content_showdesc": "short",
            "content_showatc": "1",
            "content_ajaxcart": "1",
            "content_autohidepopup": "1",
            "content_showqty": "1",
            "content_showmeta": "1",
            "content_themebtn": "0",
            "content_btncolour": "#66cc99",
            "content_btnhovcolour": "#47C285",
            "content_btntextcolour": "#ffffff",
            "content_btntexthovcolour": "#ffffff",
            "general_gallery": "1",
            "general_overlaycolour": "#000000",
            "general_overlayopacity": "0.8"
        },
        "imgsizes": {
            "catalog": {
                "width": "700",
                "height": "791",
                "crop": 1
            },
            "single": {
                "width": "700",
                "height": "791",
                "crop": 1
            },
            "thumbnail": {
                "width": "120",
                "height": "136",
                "crop": 1
            }
        },
        "url": "http:\/\/uplift.swiftideas.com",
        "text": {
            "added": "Added!",
            "adding": "Adding to Cart...",
            "loading": "Loading..."
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/jck-woo-quickview/assets/frontend/js/main.min-ver=4.9.6.js'></script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/jquery.form.min-ver=3.51.0-2014.06.20.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {
        "loaderUrl": "http:\/\/uplift.swiftideas.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif",
        "recaptchaEmpty": "Please verify that you are not a robot.",
        "sending": "Sending ...",
        "cached": "1"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts-ver=4.4.2.js'></script>
<script type='text/javascript' src='wp-content/plugins/swift-framework/includes/page-builder/frontend-assets/js/spb-functions.js'></script>
<script type='text/javascript' data-cfasync="true" src='wp-content/plugins/swift-framework/includes/swift-slider/assets/js/swift-slider.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/home\/home-classic\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View Cart",
        "cart_url": "http:\/\/uplift.swiftideas.com\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min-ver=2.5.5.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min-ver=2.70.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/home\/home-classic\/?wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min-ver=2.5.5.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min-ver=1.4.1.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/home\/home-classic\/?wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min-ver=2.5.5.js'></script>
<script type='text/javascript' src='wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min-ver=1.2.0.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var yith_wcwl_l10n = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "redirect_to_cart": "no",
        "multi_wishlist": "",
        "hide_add_button": "1",
        "is_user_logged_in": "",
        "ajax_loader_url": "http:\/\/uplift.swiftideas.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif",
        "remove_from_wishlist_after_add_to_cart": "yes",
        "labels": {
            "cookie_disabled": "We are sorry, but this feature is available only if cookies are enabled on your browser.",
            "added_to_cart_message": "<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"
        },
        "actions": {
            "add_to_wishlist_action": "add_to_wishlist",
            "remove_from_wishlist_action": "remove_from_wishlist",
            "move_to_another_wishlist_action": "move_to_another_wishlsit",
            "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl-ver=2.0.15.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/bootstrap.min.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/jquery-ui-1.10.2.custom.min.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/owl.carousel.min.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/theme-scripts.min.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/ilightbox.min.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/jquery.isotope.min.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/imagesloaded.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/combine/jquery.infinitescroll.min.js'></script>
<script type='text/javascript' src='wp-content/themes/uplift/js/functions.js'></script>
<script type='text/javascript' src='wp-includes/js/comment-reply.min-ver=4.9.6.js'></script>
<script type='text/javascript' src='wp-includes/js/hoverIntent.min-ver=1.8.1.js'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min-ver=4.9.6.js'></script>

</body>

</html>